# Simple program demonstrating symbolic execution for Linux on 64-bit Intel architecture

## Details
See the classroom exercise on Blackboard for instructions

## Reference
* JHU-ISI, Software Vulnerability Analysis, EN.650.660
* Reuben Johnston, reub@jhu.edu


